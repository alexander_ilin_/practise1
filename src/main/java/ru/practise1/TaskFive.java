package ru.practise1;

/**
 * Created by alex on 04.10.15.
 */
public class TaskFive {
    public static void main(String[] args){
        int array[] = new int[10];
        int sum = 0;
        int mul = 1;
        float med = 0.0f;
        for (int i = 0; i < 10; i++){
            array[i] = i;
        }
        int max = array[0];
        int min = array[0];
        for (int i = 0; i < 10; i++){
            sum += array[i];
            mul *= array[i];
            if (array[i] > max){
                max = array[i];
            }
            if (array[i] < min){
                min = array[i];
            }
        }

        med = (float)sum / 10;

        System.out.format("sum = %d, mul = %d, max = %d, min = %d, med = %f", sum, mul, max, min, med);
    }
}
