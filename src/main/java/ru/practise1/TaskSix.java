package ru.practise1;

/**
 * Created by alex on 04.10.15.
 */
public class TaskSix {
    public static void main(String[] args) {
        int array[] = new int[10];
        boolean increased = true;
        boolean decreased = true;

        for (int i = 0; i < 10; i++) {
            array[i] = 10-i;
        }
        for (int i = 0; i < 9; i++){
            if (array[i] < array[i+1]){
                continue;
            }
            else{
                increased = false;
            }
        }
        for (int i = 0; i < 9; i++){
            if (array[i] > array[i+1]){
                continue;
            }
            else{
                decreased = false;
            }
        }
        if (increased){
            System.out.println("Возрастает");
        }
        if (decreased){
            System.out.println("Убывает");
        }
    }
}
