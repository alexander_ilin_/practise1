package ru.practise1;

/**
 * Created by alex on 07.10.15.
 */
public class Triangle {
    private Point2D x, y, z;

    public Point2D getX() {
        return x;
    }

    public void setX(Point2D x) {
        this.x = x;
    }

    public Point2D getY() {
        return y;
    }

    public void setY(Point2D y) {
        this.y = y;
    }

    public Point2D getZ() {
        return z;
    }

    public void setZ(Point2D z) {
        this.z = z;
    }

    public Triangle(Point2D x, Point2D y, Point2D z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void getCoordinates() {
        System.out.format("x = (%d;%d), y = (%d;%d), z = (%d;%d)\n",
                getX().getX(), getX().getY(), getY().getX(), getY().getY(), getZ().getX(), getZ().getY());
    }

    public void move(int dx, int dy) {
        this.x.setX(this.x.getX() + dx);
        this.x.setY(this.x.getY() + dy);

        this.y.setX(this.y.getX() + dx);
        this.y.setY(this.y.getY() + dy);

        this.z.setX(this.z.getX() + dx);
        this.z.setY(this.z.getY() + dy);
    }

    public double getSquare() {
        double p, a, b, c;
        a = Math.sqrt(Math.pow(x.getX() - y.getX(), 2) + Math.pow(x.getY() - y.getY(), 2));
        b = Math.sqrt(Math.pow(x.getX() - z.getX(), 2) + Math.pow(x.getY() - z.getY(), 2));
        c = Math.sqrt(Math.pow(y.getX() - z.getX(), 2) + Math.pow(y.getY() - z.getY(), 2));
        p = (a + b + c) / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    public boolean isPointInside(int x, int y) {
        int a = (this.x.getX() - x) * (this.y.getY() - this.x.getY()) - (this.y.getX() - this.x.getX()) * (this.x.getY() - y);
        int b = (this.y.getX() - x) * (this.z.getY() - this.y.getY()) - (this.z.getX() - this.y.getX()) * (this.y.getY() - y);
        int c = (this.z.getX() - x) * (this.x.getY() - this.z.getY()) - (this.x.getX() - this.z.getX()) * (this.z.getY() - y);
        if (a >= 0 && b >= 0 && c >= 0 || a <= 0 && b <= 0 && c <= 0)
            return true;
        else {
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Triangle triangle = (Triangle) o;

        if (!x.equals(triangle.x)) return false;
        if (!y.equals(triangle.y)) return false;
        return z.equals(triangle.z);

    }

    @Override
    public int hashCode() {
        int result = x.hashCode();
        result = 31 * result + y.hashCode();
        result = 31 * result + z.hashCode();
        return result;
    }

    public boolean isPoint2DInside(Point2D point) {
        int a = (this.x.getX() - point.getX()) * (this.y.getY() - this.x.getY()) - (this.y.getX() - this.x.getX()) * (this.x.getY() - point.getY());
        int b = (this.y.getX() - point.getX()) * (this.z.getY() - this.y.getY()) - (this.z.getX() - this.y.getX()) * (this.y.getY() - point.getY());
        int c = (this.z.getX() - point.getX()) * (this.x.getY() - this.z.getY()) - (this.x.getX() - this.z.getX()) * (this.z.getY() - point.getY());
        if (a >= 0 && b >= 0 && c >= 0 || a <= 0 && b <= 0 && c <= 0)
            return true;
        else {
            return false;

        }
    }

    public boolean isEquilateral() {
        double a = Math.sqrt(Math.pow(x.getX() - y.getX(), 2) + Math.pow(x.getY() - y.getY(), 2));
        double b = Math.sqrt(Math.pow(x.getX() - z.getX(), 2) + Math.pow(x.getY() - z.getY(), 2));
        double c = Math.sqrt(Math.pow(y.getX() - z.getX(), 2) + Math.pow(y.getY() - z.getY(), 2));

        if (a == b && a == c) {
            return true;
        }
        else {
            return false;
        }
    }

    public static void main(String[] args) {
        Point2D x = new Point2D(10, 10);
        Point2D y = new Point2D(20, 10);
        Point2D z = new Point2D(10, 20);
        Triangle tri1 = new Triangle(x, y, z);
        System.out.format("%f\n", tri1.getSquare());
        tri1.getCoordinates();
        tri1.move(10, 30);
        tri1.getCoordinates();
        System.out.print(tri1.isPointInside(15, 16));
        System.out.println();
        System.out.print(tri1.isEquilateral());
    }
}
