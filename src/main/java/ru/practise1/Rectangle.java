package ru.practise1;

/**
 * Created by alex on 04.10.15.
 */
public class Rectangle {
    private int x1, y1, x2, y2;

    public Rectangle(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }
    public Rectangle(int height, int width)  {
        this.x1 = 0;
        this.y1 = 0;
        this.x2 = x1 + width;
        this.y2 = y1 + height;
    }
    public Rectangle() {
        this.x1 = 0;
        this.y1 = 0;
        this.x2 = 1;
        this.y2 = 1;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY1() {

        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getX1() {

        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public void getCoordinates() {
        System.out.format("x1 = %d, y1 = %d, x2 = %d, y2 = %d\n", getX1(), getY1(), getX2(), getY2());
    }

    public void move(int dx, int dy) {
        this.x1 += dx;
        this.y1 += dy;
        this.x2 += dx;
        this.y2 += dy;
    }

    public void reduce(int nx, int ny) {
        this.x2 = this.x1 + (this.x2 - this.x1) / nx;
        this.y2 = this.y1 + (this.y2 - this.y1) / ny;
    }

    public void large(int n) {
        this.x2 = this.x1 + (this.x2 - this.x1) * n;
        this.y2 = this.y1 + (this.y2 - this.y1) * n;
    }

    public int getSquare() {
        return (this.x2 - this.x1) * (this.y2 - this.y1);
    }

    public boolean isInside(int x, int y) {
        if (x > this.x1 && x < this.x2 && y > this.y1 && y < this.y2) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean isPoint2DInside(Point2D point2D) {
        int point2D_x = point2D.getX();
        int point2D_y = point2D.getY();
        if (point2D_x > this.x1 && point2D_x < this.x2 && point2D_y > this.y1 && point2D_y < this.y2) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean isCrossed(Rectangle rectangle) {
        boolean answer = true;

        if ((this.x1 > rectangle.x2 || this.x2 < rectangle.x1) && (this.y1 > rectangle.y2 || this.y2 < rectangle.y1)) {
            answer = false;
        }
        return answer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle rectangle = (Rectangle) o;

        if (x1 != rectangle.x1) return false;
        if (y1 != rectangle.y1) return false;
        if (x2 != rectangle.x2) return false;
        return y2 == rectangle.y2;

    }

    @Override
    public int hashCode() {
        int result = x1;
        result = 31 * result + y1;
        result = 31 * result + x2;
        result = 31 * result + y2;
        return result;
    }

    public boolean isNested(Rectangle rectangle) {
        boolean answer = false;

        if (this.x1 > rectangle.x1 && this.x2 < rectangle.x2 && this.y1 > rectangle.y1 && this.y2 < rectangle.y2) {
            answer = true;
        }

        return answer;

    }

    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(16, 16, 20, 20);
        Rectangle rect2 = new Rectangle(15, 15, 30, 30);
        System.out.print(rect1.isCrossed(rect2));
        System.out.println();
        System.out.print(rect1.isNested(rect2));
        System.out.println();
        Point2D point = new Point2D(21, 41);
        System.out.format("%d\n", rect1.getSquare());
        rect1.getCoordinates();
        rect1.move(10, 30);
        rect1.getCoordinates();
        rect1.reduce(5, 5);
        rect1.getCoordinates();
        rect1.large(5);
        rect1.getCoordinates();
        rect2.getCoordinates();
        System.out.print(rect1.isInside(20, 50));
        System.out.println();
        System.out.print(rect1.isPoint2DInside((point)));
    }
}