package ru.practise1;

/**
 * Created by alex on 04.10.15.
 */
public class TaskTwo {
    public static void main(String[] args){
        int a = 10;
        int b = 10;

        System.out.format("%d + %d = %d", a, b, a + b);
        System.out.println();
        System.out.format("%d * %d = %d", a, b, a * b);
        System.out.println();
        System.out.format("%d / %d = %d", a, b, a / b);
        System.out.println();
        System.out.format("%d %% %d = %d", a, b, a % b);
        System.out.println();

        if (a == b){
            System.out.println("Равны");
        }
        else {
            System.out.println("Не равны");
        }
    }
}
