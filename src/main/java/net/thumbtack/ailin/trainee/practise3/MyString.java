package net.thumbtack.ailin.trainee.practise3;

/**
 * Created by alex on 18.10.15.
 */
public class MyString {
    public static void main(String[] args) {
        String string = "This is a string";
        System.out.println(string.length());
        System.out.println(string.charAt(15));
        System.out.print(string.indexOf('T'));
        System.out.println();
        System.out.print(string.indexOf('s', 4));
        System.out.println();
        System.out.print(string.indexOf("is"));
        System.out.println();
        System.out.print(string.indexOf("is", 3));
        System.out.println();
        System.out.print(string.lastIndexOf('s'));
        System.out.println();
        System.out.print(string.lastIndexOf('s', 3));
        System.out.println();
        System.out.print(string.lastIndexOf("is"));
        System.out.println();
        System.out.print(string.lastIndexOf("is", 3));
        System.out.println();
        String string1 = "this is a string";
        System.out.print(string.equals(string1));
        System.out.println();
        System.out.print(string.equalsIgnoreCase(string1));
        System.out.println();
        System.out.print(string.compareTo(string1));
        System.out.println();
        System.out.print(string.compareToIgnoreCase(string1));
        System.out.println();
        string = string + string1;
        System.out.println(string);
        System.out.println(string+=string);
        String newString = "It's a new string";
        if (newString.startsWith("It")) {
            System.out.println("This string starts with \"It\". Good job!");
        }
        if (newString.endsWith ("string")) {
            System.out.println("This string ends with \"string\". Good job!");
        }
        System.out.println(newString.substring(5, 10));
        System.out.println(newString.substring(10));
        System.out.print(newString.getBytes());
        System.out.println();
        char[] dst = new char[10];
        newString.getChars(0, 10, dst, 0);
        System.out.println(dst);
        System.out.println(newString.replace('\'',' '));
        System.out.println(newString.replaceFirst("string", "STRING"));
        System.out.println(newString.split(" ")[0]);
        System.out.println(String.valueOf(true));
        System.out.println(String.format(newString));

    }
}
