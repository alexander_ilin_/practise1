package net.thumbtack.ailin.trainee.practise3;

import net.thumbtack.ailin.trainee.practise1.Rectangle;

/**
 * Created by alex on 18.10.15.
 */
public class Rectangle3D extends Rectangle {
    private int depth;

    public Rectangle3D(int x1, int y1, int x2, int y2, int depth) {
        super(x1, y1, x2, y2);
        this.depth = depth;
    }

    public Rectangle3D(int height, int width, int depth) {
        super(height, width);
        this.depth = depth;
    }
}
