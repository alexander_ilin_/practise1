package net.thumbtack.ailin.trainee.practise1;

/**
 * Created by alex on 10.10.15.
 */
public class Circle {
    private Point2D center;
    private int radius;

    public Circle(Point2D center, int radius){
        this.center = center;
        this.radius = radius;
    }

    public Point2D getCenter() {
        return center;
    }

    public void setCenter(Point2D center) {
        this.center = center;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public void getCoordinates() {
        System.out.format("Center = (%d, %d), radius = %d\n", this.center.getX(), this.center.getY(), this.radius);
    }

    public void move(int dx, int dy) {
        this.center.setX(this.center.getX() + dx);
        this.center.setY(this.center.getY() + dy);
    }

    public double getSquare() {
        return Math.PI * Math.pow(this.radius, 2);
    }

    public double getCircleLength() {
        return 2 * Math.PI * this.radius;
    }

    public boolean isInside(int x, int y) {
        double distance = Math.sqrt(Math.pow(this.center.getX() - x, 2) + Math.pow(this.center.getY() - y, 2));
        if (distance >= this.radius) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Circle circle = (Circle) o;

        if (radius != circle.radius) return false;
        return center.equals(circle.center);

    }

    @Override
    public int hashCode() {
        int result = center.hashCode();
        result = 31 * result + radius;
        return result;
    }

    public boolean isPoint2DInside(Point2D point) {
        double distance = Math.sqrt(Math.pow(this.center.getX() - point.getX(), 2) + Math.pow(this.center.getY() - point.getY(), 2));
        if (distance >= this.radius) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Point2D center = new Point2D(30, 30);
        Circle circle = new Circle(center, 10);
        circle.getCoordinates();
        System.out.format("%f\n", circle.getSquare());
        System.out.print(circle.isInside(35, 35));
    }
}
