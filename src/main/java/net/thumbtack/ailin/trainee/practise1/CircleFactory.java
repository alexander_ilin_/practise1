package net.thumbtack.ailin.trainee.practise1;

/**
 * Created by alex on 10.10.15.
 */
public class CircleFactory {
    static int numberOfCircles = 0;

    public static Circle createCircle(Point2D center, int radius) {
        Circle circle = new Circle(center, radius);
        numberOfCircles++;

        return circle;
    }

    public static int getNumberOfCircles() {
        return numberOfCircles;
    }

    public static void main(String[] args) {
        Circle circle1 = createCircle(new Point2D(0, 0), 5);
        circle1.getCoordinates();
        Circle circle2 = createCircle(new Point2D(5, 5), 10);
        Circle circle3 = createCircle(new Point2D(10, 0), 12);

        System.out.format("%d", getNumberOfCircles());
    }
}
