package net.thumbtack.ailin.trainee.practise1;

/**
 * Created by alex on 04.10.15.
 */
public class TaskThree {
    public static void main(String[] args){
        float af = 5.4f;
        float bf = 3.2f;

        System.out.format("%f + %f = %f", af, bf, af + bf);
        System.out.println();

        if (af == bf){
            System.out.println("Равны");
        }
        else {
            System.out.println("Не равны");
        }
    }
}
