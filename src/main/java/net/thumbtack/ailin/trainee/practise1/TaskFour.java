package net.thumbtack.ailin.trainee.practise1;

/**
 * Created by alex on 04.10.15.
 */
public class TaskFour {
    public static void main(String[] args){
        int leftCorner [] = {10,10};
        int rightCorner [] = {50, 50};
        int point [] = {40,60};

        if (point[0] > leftCorner[0] && point [0] < rightCorner[0] && point [1] > leftCorner[1] && point[1] < rightCorner[1]){
            System.out.println("Точка лежит внутри прямоугольника");
        }
        else{
            System.out.println("Точка лежит вне прямоугольника");
        }
    }
}
