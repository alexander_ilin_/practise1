package net.thumbtack.ailin.trainee.practise2;

/**
 * Created by alex on 11.10.15.
 */
public class EmailValidator {
    public boolean isValid(String email) {
        if (email == null) {
            return false;
        } else {
            return email.matches("^[^0-9\\s]\\S*@[^@]+");
        }
    }
}
