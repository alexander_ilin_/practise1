package net.thumbtack.ailin.trainee.practise2;

/**
 * Created by alex on 11.10.15.
 */
public class OperationOnEmptyList extends RuntimeException {
    public OperationOnEmptyList(String s) {
        super(s);
    }
}
