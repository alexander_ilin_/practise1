package net.thumbtack.ailin.trainee.practise2;

/**
 * Created by alex on 11.10.15.
 */
class Node {
    private int value;
    private Node prev, next;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node getPrev() {
        return prev;
    }

    public void setPrev(Node prev) {
        this.prev = prev;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node(Node prev, Node next, int value) {
        this.value = value;
        this.prev = prev;
        this.next = next;
    }
}
