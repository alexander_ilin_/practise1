package net.thumbtack.ailin.trainee.practise2;

/**
 * Created by alex on 11.10.15.
 */
public class DoubleLinkedList {
    private Node head, tail;

    public DoubleLinkedList add(int value) {
        if (this.head == null) {
            head = new Node(null, null, value);
            tail = head;
            System.out.format("Item added - %d\n", value);
        }
        else {
            tail = new Node(tail, null, value);
            tail.getPrev().setNext(tail);
            System.out.format("Item added - %d\n", value);
        }
        return this;
    }
    public int head() {
        return head.getValue();
//        throw new OperationOnEmptyList("head called on empty list");
    }

    public DoubleLinkedList tail() {
        head = head.getNext();
        if (head != null) {
            head.setPrev(null);
        }
        return this;
    }

    public DoubleLinkedList print() {
        //throw new OperationOnEmptyList("print called on empty list");

        Node tmp = head;
        while (tmp != null) {
            System.out.format("%d ", tmp.getValue());
//            System.out.println("+");
            tmp = tmp.getNext();
        }
        System.out.println();
        return this;
    }

    public static void main(String[] args) {
        DoubleLinkedList list = new DoubleLinkedList();
        list.add(10);
        list.add(11);
        list.add(12);
        list.print();
        list.tail().print();
    }
}
