package net.thumbtack.ailin.trainee.practise3;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alex on 18.10.15.
 */
public class MyStringTest {
    @Test
    public void testTheseStringsAreNotEqual() throws Exception {
        //arrange
        String str1 = "It's a string";
        String str2 = "It's another string";

        //act
        boolean actual = str1.equals(str2);

        //assert
        assertFalse(actual);
    }

    @Test
    public void testThisWillTrimTheString() throws Exception {
        //arrange
        String string = "    It's a string   ";

        //act
        String actual = string.trim();

        //assert
        assertEquals("It's a string", actual);
    }

    @Test
    public void testStringBuilder() throws Exception {
        //arrange
        StringBuilder stringbuild = new StringBuilder("It's");

        //act
        StringBuilder actual = stringbuild.append(" ");
        actual = stringbuild.append("a string");
        actual.insert(7, "long ");

        //assert
        assertEquals("It's a long string", actual.toString());
    }
}