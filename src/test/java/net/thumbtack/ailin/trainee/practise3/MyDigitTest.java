package net.thumbtack.ailin.trainee.practise3;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.Assert.*;

/**
 * Created by alex on 18.10.15.
 */
public class MyDigitTest {
    @Test
    public void testInteger() throws Exception {
        //arrange
        int x = 10;
        int newX;
        Integer MyInteger = new Integer(x);

        //act
        newX = MyInteger.intValue();

        //assert
        assertEquals(10, newX);
    }

    @Test
    public void testDouble() throws Exception {
        //arrange
        double d = 0.000213;
        double newD;
        Double MyDouble = new Double(d);

        //act
        newD = MyDouble;

        //assert
        assertEquals(0.000213, newD, 0);
    }

    @Test
    public void testBigIntegers() throws Exception {
        //arrange
        BigInteger bigint1 = new BigInteger("45000000000000000000000000000000000000");
        BigInteger bigint2 = new BigInteger("20000000000000000000000000000000000000");

        //act
        BigInteger actual = bigint1.multiply(bigint2);

        //assert
        assertEquals("900000000000000000000000000000000000000000000000000000000000000000000000000", actual.toString());
    }

    @Test
    public void testBigDecimals() throws Exception {
        //arrange
        BigDecimal bigdec1 = new BigDecimal("45E10");
        BigDecimal bigdec2 = new BigDecimal("2E10");

        //act
        BigDecimal actual = bigdec1.add(bigdec2);

        //arrange
        assertEquals("4.7E+11", actual.toString());

    }
}