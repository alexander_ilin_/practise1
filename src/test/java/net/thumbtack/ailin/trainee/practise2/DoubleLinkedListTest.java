package net.thumbtack.ailin.trainee.practise2;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alex on 11.10.15.
 */
public class DoubleLinkedListTest {

    @Test(expected = OperationOnEmptyList.class) //assert
    public void testShouldFailIfHeadHasCalledOnEmptyList() throws Exception {
        // arrange
        DoubleLinkedList list = new DoubleLinkedList();

        // act
        list.head();
    }
}