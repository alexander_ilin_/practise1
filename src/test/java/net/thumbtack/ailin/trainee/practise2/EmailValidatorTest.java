package net.thumbtack.ailin.trainee.practise2;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alex on 11.10.15.
 */
public class EmailValidatorTest {
    @Test
    public void testEmailIsInvalidIfItContainsWhiteSpaces() throws Exception {
        //arrange
        EmailValidator validator = new EmailValidator();
        String validEmail = "    \t @ \t \n";

        //act
        boolean actual = validator.isValid(validEmail);

        //assert
        assertFalse(actual);
    }

    @Test
    public void test() throws Exception {
        assertEquals(2, 1 + 1);
    }
}