package net.thumbtack.ailin.trainee.practise2;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alex on 11.10.15.
 */
public class MathUtilsTest {

    @Test
    public void testFivePlusFiveEqualsTen() throws Exception {
        // arrange
        int a = 5, b = 5;

        // act
        int actual = MathUtils.sum(a, b);

        // assert
        assertEquals(10, actual);
    }
}