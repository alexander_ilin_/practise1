package net.thumbtack.ailin.trainee.practise1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alex on 17.10.15.
 */
public class RectangleTest {
    @Test
    public void testSquareOfRectangle() throws Exception {
        //arrange
        Rectangle rect = new Rectangle(-5, 0, 10, 20);

        //act
        int actual = rect.getSquare();

        //assert
        assertEquals(300, actual);
    }

    @Test
    public void testThisPointShouldBeInside() throws Exception {
        //arrange
        Rectangle rect = new Rectangle(-5, -10, 20, 0);

        //act
        boolean actual = rect.isInside(10, -2);

        //assert
        assertTrue(actual);
    }

    @Test
    public void testThisRectanglesShouldBeCrossed() throws Exception {
        //arrange
        Rectangle rect1 = new Rectangle(5, 12, 30, 20);
        Rectangle rect2 = new Rectangle(-10, 10, 10, 25);

        //act
        boolean actual = rect1.isCrossed(rect2);

        //assert
        assertTrue(actual);
    }

    @Test
    public void testThisPointShouldNotBeInside() throws Exception {
        //arrange
        Rectangle rect = new Rectangle();

        //act
        boolean actual = rect.isInside(3, 3);

        //assert
        assertFalse(actual);
    }
}