package net.thumbtack.ailin.trainee.practise1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alex on 17.10.15.
 */
public class TriangleTest {
    @Test
    public void testTriangleSquare() throws Exception {
        //arrange
        Triangle triangle = new Triangle(new Point2D(-5,0), new Point2D(0, 10), new Point2D(5, 0));

        //act
        double actual = triangle.getSquare();

        //assert
        assertEquals((double)50, actual, 0.0000001);
    }

    @Test
    public void testThisPoint2DShouldBeInside() throws Exception {
        //arrange
        Triangle triangle = new Triangle(new Point2D(-5,0), new Point2D(0, 10), new Point2D(5, 0));

        //act
        boolean actual = triangle.isPoint2DInside(new Point2D(0,2));

        //assert
        assertTrue(actual);
    }

    @Test
    public void testThisTriangleShouldNotBeEquilateral() throws Exception {
        //arrange
        Triangle triangle = new Triangle(new Point2D(-5,0), new Point2D(0, 10), new Point2D(5, 0));

        //act
        boolean actual = triangle.isEquilateral();

        //assert
        assertFalse(actual);

    }
}